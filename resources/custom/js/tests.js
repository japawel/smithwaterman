sinon.log = function(){ console.log.apply(console, arguments); };

//gifts tests
test("test get simple gift", function(){
	var functionSpy = this.spy(window.app.gift.getGift);
	var result = functionSpy('A', 'A');
	equal(result, 1);
	result = functionSpy('C', 'C');
	equal(result, 1);
	result = functionSpy('G', 'G');
	equal(result, 1);
	result = functionSpy('T', 'T');
	equal(result, 1);
	result = functionSpy('A', 'C');
	equal(result, -1);
	result = functionSpy('A', 'G');
	equal(result, -1);
	result = functionSpy('A', 'T');
	equal(result, -1);
	result = functionSpy('C', 'G');
	equal(result, -1);
	result = functionSpy('C', 'T');
	equal(result, -1);
	result = functionSpy('G', 'T');
	equal(result, -1);
});

test("test get matrix gift", function(){
	var setSpy = this.spy(window.app.gift.setDataFromPrams);
	setSpy({
		similarity_matrix: window.app.gift.teplate_similarity_matrix, 
		simple_gift_function: false
		});
	ok(setSpy.called, "function spy was called");
	var functionSpy = this.spy(window.app.gift.getGift);
	var result = functionSpy('A', 'A');
	equal(result, 10);
	result = functionSpy('C', 'C');
	equal(result, 9);
	result = functionSpy('G', 'G');
	equal(result, 7);
	result = functionSpy('T', 'T');
	equal(result, 8);
	result = functionSpy('A', 'C');
	equal(result, -3);
	result = functionSpy('A', 'G');
	equal(result, -1);
	result = functionSpy('A', 'T');
	equal(result, -4);
	result = functionSpy('C', 'G');
	equal(result, -5);
	result = functionSpy('C', 'T');
	equal(result, 0);
	result = functionSpy('G', 'T');
	equal(result, -3);
	window.app.gift.simple_gift_function=true;
});

//punishments tests
test("punishment simple from top test", function(){
	window.app.punishment.setPunishments({begin_punishment: -1, continue_punishment: -1});
	var functionSpy = this.spy(window.app.punishment.getPunishment);
	var matrix=[[{max_from_top: true},{max_from_top: true}],
				[{max_from_top: false},{max_from_top: false}],
				[{max_from_top: false},{}]];
	var result;
	for (var x = 0; x < 3; x++)
	{
		for (var y = 0; y < 2; y++)
		{
			result=functionSpy(x, y, "top", matrix);
			equal(result, -1);
		}
	}	
});

test("punishment affine from left test", function(){
	window.app.punishment.setPunishments({begin_punishment: -5, continue_punishment: -1});
	var functionSpy = this.spy(window.app.punishment.getPunishment);
	var matrix=[[{max_from_left: true},{max_from_left: true}],
				[{max_from_left: false},{max_from_left: false}],
				[{max_from_left: false},{}]];
	var result;
	for (var x = 0; x < 3; x++)
	{
		for (var y = 0; y < 2; y++)
		{
			result=functionSpy(x, y, "left", matrix);
			if (x == 1)
				equal(result, -1);
			else
				equal(result, -5);
		}
	}	
});

test("punishment affine from top test", function(){
	window.app.punishment.setPunishments({begin_punishment: -5, continue_punishment: -1});
	var functionSpy = this.spy(window.app.punishment.getPunishment);
	var matrix=[[{max_from_top: true},{max_from_top: true}],
				[{max_from_top: false},{max_from_top: false}],
				[{max_from_top: false},{}]];
	var result;
	for (var x = 0; x < 3; x++)
	{
		for (var y = 0; y < 2; y++)
		{
			result=functionSpy(x, y, "top", matrix);
			if (x == 0 && y == 1)
				equal(result, -1);
			else
				equal(result, -5);
		}
	}	
});

test("punishment simple from left test", function(){
	window.app.punishment.setPunishments({begin_punishment: -1, continue_punishment: -1});
	var functionSpy = this.spy(window.app.punishment.getPunishment);
	var matrix=[[{max_from_left: true},{max_from_left: true}],
				[{max_from_left: false},{max_from_left: false}],
				[{max_from_left: false},{}]];
	var result;
	for (var x = 0; x < 3; x++)
	{
		for (var y = 0; y < 2; y++)
		{
			result=functionSpy(x, y, "left", matrix);
			equal(result, -1);
		}
	}	
});


//moves tests
test("test should call function spy", function () {
	window.app.smithWaterman.init({first:"ACGT", secound:"ACCGT"});
	var functionSpy = this.spy(window.app.smithWaterman.move);
	var result = functionSpy(true);
	var matrix = new Array(window.app.smithWaterman.sequences.first.length);
	for (var i = 0; i < window.app.smithWaterman.sequences.first.length; ++i)
	{
		matrix[i] = new Array(window.app.smithWaterman.sequences.secound.length);
		for (var j = 0; j < window.app.smithWaterman.sequences.secound.length; ++j)
		{
			matrix[i][j] = new Object();
		}
	}
	var firstObj = new Object;
	firstObj.from_diagonal = 1;
	firstObj.from_left = -1;
	firstObj.from_top = -1;
	firstObj.max = 1;
	firstObj.max_from_diagonal = true;
	firstObj.gift = 1;
	firstObj.left_punishment = -1;
	firstObj.top_punishment = -1;
	matrix[0][0] = firstObj;
	deepEqual(matrix, result);
	var matrix=
		   [[{from_diagonal:  1, from_top: -1, from_left: -1, max:1, gift:  1, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true},
			 {from_diagonal: -1, from_top:  0, from_left: -1, max:0, gift: -1, left_punishment: -1, top_punishment: -1, max_from_top: 		true},
			 {from_diagonal: -1, from_top: -1, from_left: -1, max:0, gift: -1, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true, max_from_top: true, max_from_left: true},
			 {from_diagonal: -1, from_top: -1, from_left: -1, max:0, gift: -1, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true, max_from_top: true, max_from_left: true},
			 {from_diagonal: -1, from_top: -1, from_left: -1, max:0, gift: -1, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true, max_from_top: true, max_from_left: true}],
			[{from_diagonal: -1, from_top: -1, from_left:  0, max:0, gift: -1, left_punishment: -1, top_punishment: -1, max_from_left: 		true},
			 {from_diagonal:  2, from_top: -1, from_left: -1, max:2, gift:  1, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true},
			 {from_diagonal:  1, from_top:  1, from_left: -1, max:1, gift:  1, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true, max_from_top: true},
			 {from_diagonal: -1, from_top:  0, from_left: -1, max:0, gift: -1, left_punishment: -1, top_punishment: -1, max_from_top: 		true},
			 {from_diagonal: -1, from_top: -1, from_left: -1, max:0, gift: -1, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true, max_from_top: true, max_from_left: true}],
			[{from_diagonal: -1, from_top: -1, from_left: -1, max:0, gift: -1, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true, max_from_top: true, max_from_left: true},
			 {from_diagonal: -1, from_top: -1, from_left:  1, max:1, gift: -1, left_punishment: -1, top_punishment: -1, max_from_left: 		true},
			 {from_diagonal:  1, from_top:  0, from_left:  0, max:1, gift: -1, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true},
			 {from_diagonal:  2, from_top:  0, from_left: -1, max:2, gift:  1, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true},
			 {from_diagonal: -1, from_top:  1, from_left: -1, max:1, gift: -1, left_punishment: -1, top_punishment: -1, max_from_top: 		true}],
			[{from_diagonal: -1, from_top: -1, from_left: -1, max:0, gift: -1, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true, max_from_top: true, max_from_left: true},
			 {from_diagonal: -1, from_top: -1, from_left:  0, max:0, gift: -1, left_punishment: -1, top_punishment: -1, max_from_left: 		true},
			 {from_diagonal:  0, from_top: -1, from_left:  0, max:0, gift: -1, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true, max_from_left:true},
			 {from_diagonal:  0, from_top: -1, from_left:  1, max:1, gift: -1, left_punishment: -1, top_punishment: -1, max_from_left: 		true},
			 {from_diagonal:  3, from_top:  0, from_left:  0, max:3, gift:  1, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true}]];
	result = functionSpy();
	deepEqual(matrix, result);
	ok(functionSpy.called, "function spy was called");
});

test("test should call function spy", function () {
	window.app.smithWaterman.init({first:"ACGT", secound:"ACCGT"});
	window.app.gift.setDataFromPrams({
										simple_fit: 10,
										simple_mismatch: -3,
										simple_gift_function: true
									});
	var functionSpy = this.spy(window.app.smithWaterman.move);
	var result = functionSpy(true);
	var matrix=
		   [[{from_diagonal: 10, from_top: -1, from_left: -1, max:10, gift: 10, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true},
			 {from_diagonal: -3, from_top:  9, from_left: -1, max: 9, gift: -3, left_punishment: -1, top_punishment: -1, max_from_top: 			true},
			 {from_diagonal: -3, from_top:  8, from_left: -1, max: 8, gift: -3, left_punishment: -1, top_punishment: -1, max_from_top: 			true},
			 {from_diagonal: -3, from_top:  7, from_left: -1, max: 7, gift: -3, left_punishment: -1, top_punishment: -1, max_from_top: 			true},
			 {from_diagonal: -3, from_top:  6, from_left: -1, max: 6, gift: -3, left_punishment: -1, top_punishment: -1, max_from_top: 			true}],
			[{from_diagonal: -3, from_top: -1, from_left:  9, max: 9, gift: -3, left_punishment: -1, top_punishment: -1, max_from_left: 		true},
			 {from_diagonal: 20, from_top:  8, from_left:  8, max:20, gift: 10, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true},
			 {from_diagonal: 19, from_top: 19, from_left:  7, max:19, gift: 10, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true, max_from_top: true},
			 {from_diagonal:  5, from_top: 18, from_left:  6, max:18, gift: -3, left_punishment: -1, top_punishment: -1, max_from_top: 			true},
			 {from_diagonal:  4, from_top: 17, from_left:  5, max:17, gift: -3, left_punishment: -1, top_punishment: -1, max_from_top: 			true}],
			[{from_diagonal: -3, from_top: -1, from_left:  8, max: 8, gift: -3, left_punishment: -1, top_punishment: -1, max_from_left: 		true},
			 {from_diagonal:  6, from_top:  7, from_left: 19, max:19, gift: -3, left_punishment: -1, top_punishment: -1, max_from_left: 		true},
			 {from_diagonal: 17, from_top: 18, from_left: 18, max:18, gift: -3, left_punishment: -1, top_punishment: -1, max_from_left:		 	true, max_from_top: true},
			 {from_diagonal: 29, from_top: 17, from_left: 17, max:29, gift: 10, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true},
			 {from_diagonal: 15, from_top: 28, from_left: 16, max:28, gift: -3, left_punishment: -1, top_punishment: -1, max_from_top: 			true}],
			[{from_diagonal: -3, from_top: -1, from_left:  7, max: 7, gift: -3, left_punishment: -1, top_punishment: -1, max_from_left: 		true},
			 {from_diagonal:  5, from_top:  6, from_left: 18, max:18, gift: -3, left_punishment: -1, top_punishment: -1, max_from_left: 		true},
			 {from_diagonal: 16, from_top: 17, from_left: 17, max:17, gift: -3, left_punishment: -1, top_punishment: -1, max_from_left:		 	true, max_from_top: true},
			 {from_diagonal: 15, from_top: 16, from_left: 28, max:28, gift: -3, left_punishment: -1, top_punishment: -1, max_from_left: 		true},
			 {from_diagonal: 39, from_top: 27, from_left: 27, max:39, gift: 10, left_punishment: -1, top_punishment: -1, max_from_diagonal: 	true}]];
	result = functionSpy();
	deepEqual(matrix, result);
	ok(functionSpy.called, "function spy was called");
});

test("test should call function spy", function () {
	window.app.smithWaterman.init({first:"ACGT", secound:"ACCGT"});
	window.app.punishment.setPunishments({
										begin_punishment: -5
									});
	var setSpy = this.spy(window.app.gift.setDataFromPrams);
	setSpy({
		similarity_matrix: window.app.gift.teplate_similarity_matrix, 
		simple_gift_function: false
		});
	ok(setSpy.called, "function spy was called");
	var functionSpy = this.spy(window.app.smithWaterman.move);
	var result;
	var matrix=
		   [[{from_diagonal: 10, from_top: -5, from_left: -5, max:10, gift: 10, left_punishment: -5, top_punishment: -5, max_from_diagonal: 	true},
			 {from_diagonal: -3, from_top:  5, from_left: -5, max: 5, gift: -3, left_punishment: -5, top_punishment: -5, max_from_top: 			true},
			 {from_diagonal: -3, from_top:  0, from_left: -5, max: 0, gift: -3, left_punishment: -5, top_punishment: -5, max_from_top: 			true},
			 {from_diagonal: -1, from_top: -5, from_left: -5, max: 0, gift: -1, left_punishment: -5, top_punishment: -5, max_from_diagonal:		true},
			 {from_diagonal: -4, from_top: -5, from_left: -5, max: 0, gift: -4, left_punishment: -5, top_punishment: -5, max_from_diagonal:		true}],
			[{from_diagonal: -3, from_top: -5, from_left:  5, max: 5, gift: -3, left_punishment: -5, top_punishment: -5, max_from_left: 		true},
			 {from_diagonal: 19, from_top:  0, from_left:  0, max:19, gift:  9, left_punishment: -5, top_punishment: -5, max_from_diagonal: 	true},
			 {from_diagonal: 14, from_top: 14, from_left: -5, max:14, gift:  9, left_punishment: -5, top_punishment: -5, max_from_diagonal: 	true, max_from_top: true},
			 {from_diagonal: -5, from_top:  9, from_left: -5, max: 9, gift: -5, left_punishment: -5, top_punishment: -5, max_from_top: 			true},
			 {from_diagonal:  0, from_top:  4, from_left: -5, max: 4, gift:  0, left_punishment: -5, top_punishment: -5, max_from_top: 			true}],
			[{from_diagonal: -1, from_top: -5, from_left:  0, max: 0, gift: -1, left_punishment: -5, top_punishment: -5, max_from_left: 		true},
			 {from_diagonal:  0, from_top: -5, from_left: 14, max:14, gift: -5, left_punishment: -5, top_punishment: -5, max_from_left: 		true},
			 {from_diagonal: 14, from_top:  9, from_left:  9, max:14, gift: -5, left_punishment: -5, top_punishment: -5, max_from_diagonal:	 	true},
			 {from_diagonal: 21, from_top:  9, from_left:  4, max:21, gift:  7, left_punishment: -5, top_punishment: -5, max_from_diagonal: 	true},
			 {from_diagonal:  6, from_top: 16, from_left: -1, max:16, gift: -3, left_punishment: -5, top_punishment: -5, max_from_top: 			true}],
			[{from_diagonal: -4, from_top: -5, from_left: -5, max: 0, gift: -4, left_punishment: -5, top_punishment: -5, max_from_diagonal: 		true},
			 {from_diagonal:  0, from_top: -5, from_left:  9, max: 9, gift:  0, left_punishment: -5, top_punishment: -5, max_from_left:			true},
			 {from_diagonal: 14, from_top:  4, from_left:  9, max:14, gift:  0, left_punishment: -5, top_punishment: -5, max_from_diagonal:		true},
			 {from_diagonal: 11, from_top:  9, from_left: 16, max:16, gift: -3, left_punishment: -5, top_punishment: -5, max_from_left: 		true},
			 {from_diagonal: 29, from_top: 11, from_left: 11, max:29, gift:  8, left_punishment: -5, top_punishment: -5, max_from_diagonal: 	true}]];
	result = functionSpy();
	deepEqual(matrix, result);
});

test("test should call function spy", function () {
	window.app.smithWaterman.init({first:"AGCCCT", secound:"AGCT"});
	window.app.punishment.setPunishments({
										begin_punishment: -5,
										continue_punishment: -2
									});
	window.app.gift.setDataFromPrams({
										simple_fit: 2,
										simple_mismatch: -2, 
										simple_gift_function: true
									});
	var functionSpy = this.spy(window.app.smithWaterman.move);
	var result;
	var matrix=
		   [[{from_diagonal:  2, from_top: -5, from_left: -5, max: 2, gift:  2, left_punishment: -5, top_punishment: -5, max_from_diagonal: 	true},
			 {from_diagonal: -2, from_top: -3, from_left: -5, max: 0, gift: -2, left_punishment: -5, top_punishment: -5, max_from_diagonal: 	true},
			 {from_diagonal: -2, from_top: -5, from_left: -5, max: 0, gift: -2, left_punishment: -5, top_punishment: -5, max_from_diagonal: 	true},
			 {from_diagonal: -2, from_top: -5, from_left: -5, max: 0, gift: -2, left_punishment: -5, top_punishment: -5, max_from_diagonal:		true}],
			[{from_diagonal: -2, from_top: -5, from_left: -3, max: 0, gift: -2, left_punishment: -5, top_punishment: -5, max_from_diagonal: 	true},
			 {from_diagonal:  4, from_top: -5, from_left: -5, max: 4, gift:  2, left_punishment: -5, top_punishment: -5, max_from_diagonal: 	true},
			 {from_diagonal: -2, from_top: -1, from_left: -5, max: 0, gift: -2, left_punishment: -5, top_punishment: -5, max_from_top:		 	true},
			 {from_diagonal: -2, from_top: -2, from_left: -5, max: 0, gift: -2, left_punishment: -5, top_punishment: -2, max_from_diagonal: 	true, max_from_top:		 	true}],
			[{from_diagonal: -2, from_top: -5, from_left: -5, max: 0, gift: -2, left_punishment: -5, top_punishment: -5, max_from_diagonal:	 	true},
			 {from_diagonal: -2, from_top: -5, from_left: -1, max: 0, gift: -2, left_punishment: -5, top_punishment: -5, max_from_left:		 	true},
			 {from_diagonal:  6, from_top: -5, from_left: -5, max: 6, gift:  2, left_punishment: -5, top_punishment: -5, max_from_diagonal:	 	true},
			 {from_diagonal: -2, from_top:  1, from_left: -5, max: 1, gift: -2, left_punishment: -5, top_punishment: -5, max_from_top:		 	true}],
			[{from_diagonal: -2, from_top: -5, from_left: -5, max: 0, gift: -2, left_punishment: -5, top_punishment: -5, max_from_diagonal: 	true},
			 {from_diagonal: -2, from_top: -5, from_left: -2, max: 0, gift: -2, left_punishment: -2, top_punishment: -5, max_from_diagonal: 	true, max_from_left:		 true},
			 {from_diagonal:  2, from_top: -5, from_left:  1, max: 2, gift:  2, left_punishment: -5, top_punishment: -5, max_from_diagonal:	 	true},
			 {from_diagonal:  4, from_top: -3, from_left: -4, max: 4, gift: -2, left_punishment: -5, top_punishment: -5, max_from_diagonal: 	true}],
			[{from_diagonal: -2, from_top: -5, from_left: -5, max: 0, gift: -2, left_punishment: -5, top_punishment: -5, max_from_diagonal: 	true},
			 {from_diagonal: -2, from_top: -5, from_left: -2, max: 0, gift: -2, left_punishment: -2, top_punishment: -5, max_from_diagonal: 	true, max_from_left:		 true},
			 {from_diagonal:  2, from_top: -5, from_left: -3, max: 2, gift:  2, left_punishment: -5, top_punishment: -5, max_from_diagonal:	 	true},
			 {from_diagonal:  0, from_top: -3, from_left: -1, max: 0, gift: -2, left_punishment: -5, top_punishment: -5, max_from_diagonal: 	true}],
			[{from_diagonal: -2, from_top: -5, from_left: -5, max: 0, gift: -2, left_punishment: -5, top_punishment: -5, max_from_diagonal: 	true},
			 {from_diagonal: -2, from_top: -5, from_left: -2, max: 0, gift: -2, left_punishment: -2, top_punishment: -5, max_from_diagonal: 	true, max_from_left:		 true},
			 {from_diagonal: -2, from_top: -5, from_left: -3, max: 0, gift: -2, left_punishment: -5, top_punishment: -5, max_from_diagonal:		true},
			 {from_diagonal:  4, from_top: -5, from_left: -5, max: 4, gift:  2, left_punishment: -5, top_punishment: -5, max_from_diagonal: 	true}]];
	result = functionSpy();
	deepEqual(matrix, result);
});
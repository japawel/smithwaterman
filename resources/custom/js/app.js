﻿(function($, undefined)
{
	window.app=
	{
		init: function()
		{	
			window.view.init();
		},
		
		system: 
		{
			showInfoDialog: function(message)
			{
				$('.ui-loader').css('display','none');
				
				if(message instanceof Object)
				{  
					window.view.gui.system.popupInfo.message.text(message.description);
				}
				else
				{
					window.view.gui.system.popupInfo.message.text(message);
				}
				
				window.view.gui.system.popupInfo.whole.popup('open');
				window.view.gui.system.popupInfo.btn_ok.focus();
			},
		},
		
		smithWaterman:
		{
			info: null,
			state:
			{
				xsize: 0,
				ysize: 0,
				moveno: 0,
				allmoves: false,
				pathfound: false,
			},
			
			sequences:
			{
				first: [],
				secound: [],
			},
			
			path:
			{
				/*actualx: 0,
				actualy: 0,
				actualpos: 0,
				path: [],*/
				nexts: [],
				actualx: 0,
				actualy: 0,
				i: -1,
			},
			
			matrix: [],
			
			init: function(params)
			{
				window.app.smithWaterman.sequences.first = params.first.split("");
				window.app.smithWaterman.sequences.secound = params.secound.split("");
				window.app.smithWaterman.matrix = new Array(window.app.smithWaterman.sequences.first.length);
				for (var i = 0; i < window.app.smithWaterman.sequences.first.length; ++i)
				{
					window.app.smithWaterman.matrix[i] = new Array(window.app.smithWaterman.sequences.secound.length);
					for (var j = 0; j < window.app.smithWaterman.sequences.secound.length; ++j)
					{
						window.app.smithWaterman.matrix[i][j] = new Object();
					}
				}
				window.app.smithWaterman.state.xsize=window.app.smithWaterman.sequences.first.length;
				window.app.smithWaterman.state.ysize=window.app.smithWaterman.sequences.secound.length;
				window.app.smithWaterman.state.moveno=0;
				window.app.smithWaterman.state.allmoves=false;
				window.app.smithWaterman.state.pathfound=false;
				window.app.smithWaterman.path.nexts = new Array();
				window.app.smithWaterman.path.i = -1;
			},
			
/*functions to count matrix values*/
			countFieldFromDiagonal: function(i, j)
			{
				var gift = window.app.gift.getGift(window.app.smithWaterman.sequences.first[i], window.app.smithWaterman.sequences.secound[j])
				window.app.smithWaterman.matrix[i][j].gift = gift;
				if (i == 0 || j == 0)
				{
					window.app.smithWaterman.matrix[i][j].from_diagonal = gift;
				} else
				{
					window.app.smithWaterman.matrix[i][j].from_diagonal =  window.app.smithWaterman.matrix[i-1][j-1].max
							+ gift;
				}
			},
			
			countFieldFromLeft: function(i, j)
			{
				var punishment = window.app.punishment.getPunishment(i, j, 'left', window.app.smithWaterman.matrix)
				window.app.smithWaterman.matrix[i][j].left_punishment=punishment;
				if (i == 0)
				{
					window.app.smithWaterman.matrix[i][j].from_left = punishment;
				} else
				{
					window.app.smithWaterman.matrix[i][j].from_left =  window.app.smithWaterman.matrix[i-1][j].max
							+ punishment;
				}
			},
			
			countFieldFromTop: function(i, j)
			{
				var punishment = window.app.punishment.getPunishment(i, j, 'top', window.app.smithWaterman.matrix)
				window.app.smithWaterman.matrix[i][j].top_punishment=punishment;
				if (j == 0)
				{
					window.app.smithWaterman.matrix[i][j].from_top = punishment;
				} else
				{
					window.app.smithWaterman.matrix[i][j].from_top =  window.app.smithWaterman.matrix[i][j-1].max
							+ punishment;
				}
			},
			
			setMaxFromValues: function(max, i, j)
			{
				if(max == window.app.smithWaterman.matrix[i][j].from_left)
				{
					window.app.smithWaterman.matrix[i][j].max_from_left=true;
				}
				
				if(max == window.app.smithWaterman.matrix[i][j].from_top)
				{
					window.app.smithWaterman.matrix[i][j].max_from_top=true;
				}
				
				if(max == window.app.smithWaterman.matrix[i][j].from_diagonal)
				{
					window.app.smithWaterman.matrix[i][j].max_from_diagonal=true;
				}
			},
			
			countField: function(i, j)
			{
				window.app.smithWaterman.countFieldFromDiagonal(i, j);
				window.app.smithWaterman.countFieldFromLeft(i, j);
				window.app.smithWaterman.countFieldFromTop(i, j);
				
				var max = Math.max(window.app.smithWaterman.matrix[i][j].from_left
								,window.app.smithWaterman.matrix[i][j].from_top
								,window.app.smithWaterman.matrix[i][j].from_diagonal);
								
				window.app.smithWaterman.setMaxFromValues(max, i, j);
				
				if (max < 0)
					window.app.smithWaterman.matrix[i][j].max = 0;
				else
					window.app.smithWaterman.matrix[i][j].max = max;
			},
			
			move: function(single_move)
			{
				for (var no = window.app.smithWaterman.state.moveno; 
						no < (window.app.smithWaterman.state.xsize)*(window.app.smithWaterman.state.ysize);
						++no)
				{
					var x = no%window.app.smithWaterman.state.xsize;
					var y = Math.floor(no/window.app.smithWaterman.state.xsize);
					window.app.smithWaterman.state.moveno++;
					window.app.smithWaterman.countField(x, y);
					if (single_move)
					{
						break;
					}
				}
				
				if (window.app.smithWaterman.state.moveno == (window.app.smithWaterman.state.xsize)*(window.app.smithWaterman.state.ysize))
					window.app.smithWaterman.state.allmoves = true;
					
				return window.app.smithWaterman.matrix;
			},

/*functions to find path*/
			addPathElem: function(path_elem, i, j)
			{
				window.app.smithWaterman.matrix[i][j].path.push(path_elem);
				window.app.smithWaterman.matrix[i][j].first_paths = path_elem.first_paths;
				window.app.smithWaterman.matrix[i][j].secound_paths = path_elem.secound_paths;
				if (path_elem.tox >= 0 && path_elem.toy >= 0)
				{
					window.app.smithWaterman.matrix[path_elem.tox][path_elem.toy].i=path_elem.tox;
					window.app.smithWaterman.matrix[path_elem.tox][path_elem.toy].j=path_elem.toy;
					window.app.smithWaterman.path.nexts.push(window.app.smithWaterman.matrix[path_elem.tox][path_elem.toy]);
					if (!window.app.smithWaterman.matrix[path_elem.tox][path_elem.toy].prev_first_paths)
					{
						window.app.smithWaterman.matrix[path_elem.tox][path_elem.toy].prev_first_paths = new Array();
						window.app.smithWaterman.matrix[path_elem.tox][path_elem.toy].prev_secound_paths = new Array();
					}
					window.app.smithWaterman.matrix[path_elem.tox][path_elem.toy].prev_first_paths = window.app.smithWaterman.matrix[path_elem.tox][path_elem.toy].prev_first_paths.concat(path_elem.first_paths);
					window.app.smithWaterman.matrix[path_elem.tox][path_elem.toy].prev_secound_paths = window.app.smithWaterman.matrix[path_elem.tox][path_elem.toy].prev_secound_paths.concat(path_elem.secound_paths);
				}
			},
			
			addFirstElementIfNeed: function()
			{
				if (window.app.smithWaterman.path.nexts.length == 0)
				{
					window.app.smithWaterman.path.i=0;
					var i = window.app.smithWaterman.sequences.first.length-1;
					var j = window.app.smithWaterman.sequences.secound.length-1;
					window.app.smithWaterman.matrix[i][j].i=i;
					window.app.smithWaterman.matrix[i][j].j=j;
					window.app.smithWaterman.path.nexts.push(window.app.smithWaterman.matrix[i][j]);
					window.app.smithWaterman.matrix[i][j].prev_first_paths = new Array();
					window.app.smithWaterman.matrix[i][j].prev_first_paths.push("");
					window.app.smithWaterman.matrix[i][j].prev_secound_paths = new Array();
					window.app.smithWaterman.matrix[i][j].prev_secound_paths.push("");
				}
			},
						
			getMoveInfo: function(i, j)
			{
				var info = "<strong>Wartość po przekątnej: " + window.app.smithWaterman.matrix[i][j].from_diagonal + "</strong>"
							+ " (Nagroda za dopasowanie: " + window.app.smithWaterman.matrix[i][j].gift + ")"
							+ "<br/><strong>Wartość z góry: " + window.app.smithWaterman.matrix[i][j].from_top + "</strong>"
							+ " (Kara za przerwę z góry: " + window.app.smithWaterman.matrix[i][j].top_punishment + ")"
							+ "<br/><strong>Wartość z lewej: " + window.app.smithWaterman.matrix[i][j].from_left + "</strong>"
							+ " (Kara za przerwę z lewej: " + window.app.smithWaterman.matrix[i][j].left_punishment + ")"
							+ "<br/><strong>Maksymalna wartość z: </strong>"
							+ (window.app.smithWaterman.matrix[i][j].max_from_diagonal ? "przekątnej, " : "")
							+ (window.app.smithWaterman.matrix[i][j].max_from_top ? "góry, " : "")
							+ (window.app.smithWaterman.matrix[i][j].max_from_left ? "lewej, " : "");
				info = info.substring(0, info.length-2);
				return info;
			},
			
			find_path: function(single_move)
			{
				window.app.smithWaterman.addFirstElementIfNeed();
				
				var i;
				var j;				
				for (; window.app.smithWaterman.path.i < window.app.smithWaterman.path.nexts.length; )
				{
					i = window.app.smithWaterman.path.nexts[window.app.smithWaterman.path.i].i;
					j = window.app.smithWaterman.path.nexts[window.app.smithWaterman.path.i].j;
					if (window.app.smithWaterman.matrix[i][j].path != undefined)
					{
						for (var k = window.app.smithWaterman.matrix[i][j].first_paths.length; k < window.app.smithWaterman.matrix[i][j].prev_first_paths.length; ++k)
						{
							var first_path = window.app.smithWaterman.sequences.first[i] + window.app.smithWaterman.matrix[i][j].prev_first_paths[k];
							var secound_path = window.app.smithWaterman.sequences.secound[j] + window.app.smithWaterman.matrix[i][j].prev_secound_paths[k];
							window.app.smithWaterman.matrix[i][j].first_paths.push(first_path);
							window.app.smithWaterman.matrix[i][j].secound_paths.push(secound_path);
							if (window.app.smithWaterman.matrix[i][j].max_from_diagonal)
								window.app.smithWaterman.path.nexts.push(window.app.smithWaterman.matrix[i-1][j-1]);
							if (window.app.smithWaterman.matrix[i][j].max_from_top)
								window.app.smithWaterman.path.nexts.push(window.app.smithWaterman.matrix[i][j-1]);
							if (window.app.smithWaterman.matrix[i][j].max_from_left)
								window.app.smithWaterman.path.nexts.push(window.app.smithWaterman.matrix[i-1][j]);
						}
						window.app.smithWaterman.path.i++;
						continue;
					}
					
					window.app.smithWaterman.matrix[i][j].path = new Array();
//					var paths = new Array

					if (window.app.smithWaterman.path.nexts[window.app.smithWaterman.path.i].max_from_left)
					{
						var path_elem = new Object();
						path_elem.tox = i-1;
						path_elem.toy = j;
						path_elem.first_paths = new Array();
						path_elem.secound_paths = new Array();
						for (var k = 0; k < window.app.smithWaterman.matrix[i][j].prev_first_paths.length; ++k)
						{
							var first_path = window.app.smithWaterman.sequences.first[i] + window.app.smithWaterman.matrix[i][j].prev_first_paths[k];
							var secound_path = '–' + window.app.smithWaterman.matrix[i][j].prev_secound_paths[k];
							path_elem.first_paths.push(first_path);
							path_elem.secound_paths.push(secound_path);
						}
						window.app.smithWaterman.addPathElem(path_elem, i, j);
					}
					
					if (window.app.smithWaterman.path.nexts[window.app.smithWaterman.path.i].max_from_top)
					{
						var path_elem = new Object();
						path_elem.tox = i;
						path_elem.toy = j-1;
						path_elem.first_paths = new Array();
						path_elem.secound_paths = new Array();
						for (var k = 0; k < window.app.smithWaterman.matrix[i][j].prev_first_paths.length; ++k)
						{
							var first_path = '–' + window.app.smithWaterman.matrix[i][j].prev_first_paths[k];
							var secound_path = window.app.smithWaterman.sequences.secound[j] + window.app.smithWaterman.matrix[i][j].prev_secound_paths[k];
							path_elem.first_paths.push(first_path);
							path_elem.secound_paths.push(secound_path);
						}
						window.app.smithWaterman.addPathElem(path_elem, i, j);
					}
					
					if (window.app.smithWaterman.path.nexts[window.app.smithWaterman.path.i].max_from_diagonal)
					{
						var path_elem = new Object();
						path_elem.tox = i-1;
						path_elem.toy = j-1;
						path_elem.first_paths = new Array();
						path_elem.secound_paths = new Array();
						for (var k = 0; k < window.app.smithWaterman.matrix[i][j].prev_first_paths.length; ++k)
						{
							var first_path = window.app.smithWaterman.sequences.first[i] + window.app.smithWaterman.matrix[i][j].prev_first_paths[k];
							var secound_path = window.app.smithWaterman.sequences.secound[j] + window.app.smithWaterman.matrix[i][j].prev_secound_paths[k];
							path_elem.first_paths.push(first_path);
							path_elem.secound_paths.push(secound_path);
						}
						
						window.app.smithWaterman.addPathElem(path_elem, i, j);
					}
					
					window.app.smithWaterman.path.i++;
					if (single_move)
						break;
				}
				
				if (window.app.smithWaterman.path.i == window.app.smithWaterman.path.nexts.length)
				{
					window.app.smithWaterman.state.pathfound=true;
				}
			}
		},

/* functions related with punishments */
		punishment:
		{
			begin_punishment: -1,
			continue_punishment: -1,
			
			setPunishments: function(params)
			{
				if (params.begin_punishment != undefined)
				{
					window.app.punishment.begin_punishment = params.begin_punishment;
				} else 
				{
					window.app.punishment.begin_punishment = -1;
				}
				
				if (params.continue_punishment != undefined)
				{
					window.app.punishment.continue_punishment = params.continue_punishment;
				} else 
				{
					window.app.punishment.continue_punishment = window.app.punishment.begin_punishment;
				}
			},
			
			getPunishment: function(i, j, from, matrix)
			{
				if (window.app.punishment.begin_punishment == window.app.punishment.continue_punishment)
					return window.app.punishment.begin_punishment;
					
				if(from == 'top')
				{
					if (j == 0)
						return window.app.punishment.begin_punishment;
					
					if (matrix[i][j-1].max_from_top)
						return window.app.punishment.continue_punishment;
						
					return window.app.punishment.begin_punishment;
				}
				
				if(from == 'left')
				{
					if (i == 0)
						return window.app.punishment.begin_punishment;
					
					if (matrix[i-1][j].max_from_left)
						return window.app.punishment.continue_punishment;
						
					return window.app.punishment.begin_punishment;
				}
			}
		},

/* functions related with prize for fit */	
		gift:
		{
			//parameters witch can be transfer with setDataFromParams
			simple_gift_function: true,
			simple_fit: 1,
			simple_mismatch: -1,
			similarity_matrix : [[ 1, -1, -1, -1],
								 [-1,  1, -1, -1],
								 [-1, -1,  1, -1],
								 [-1, -1, -1,  1]],
			similarity_vecotr: ['A', 'G', 'C', 'T'],
			
			//templates for matrixes
			basic_similarity_matrix: [[ 1, -1, -1, -1],
									  [-1,  1, -1, -1],
									  [-1, -1,  1, -1],
									  [-1, -1, -1,  1]],
			teplate_similarity_matrix: [[10, -1, -3, -4],
									    [-1,  7, -5, -3],
									    [-3, -5,  9,  0],
									    [-4, -3,  0,  8]],

			setDataFromPrams: function(params)
			{
				if (params.similarity_matrix != undefined)
					window.app.gift.similarity_matrix = params.similarity_matrix;
				else
					window.app.gift.similarity_matrix = window.app.gift.basic_similarity_matrix;
					
				if (params.simple_gift_function != undefined)
					window.app.gift.simple_gift_function = params.simple_gift_function;
				else
					window.app.gift.simple_gift_function = true;
				
				if (params.simple_fit != undefined)
					window.app.gift.simple_fit = params.simple_fit;
				else
					window.app.gift.simple_fit = 1;
					
				if (params.simple_mismatch != undefined)
					window.app.gift.simple_mismatch = params.simple_mismatch;
				else
					window.app.gift.simple_mismatch = -1;
				
				if (params.similarity_vecotr != undefined)
					window.app.gift.similarity_vecotr = params.similarity_vecotr;
				else
					window.app.gift.similarity_vecotr = ['A', 'G', 'C', 'T'];			
			},
			
			getGift: function(i, j)
			{		
				if(window.app.gift.simple_gift_function)
				{
					return window.app.gift.getSimpleGift(i, j);
				}
				
				return window.app.gift.getGiftFromMatrix(i, j);
			},
			
			getSimpleGift: function(i, j)
			{
				if (i == j)
					return window.app.gift.simple_fit;
					
				return window.app.gift.simple_mismatch;
			},
			
			getGiftFromMatrix: function(i, j)
			{
				var x = -1;
				var y = -1;
				
				
				for (var idx = 0; idx < window.app.gift.similarity_vecotr.length; idx++)
				{
					if (i == window.app.gift.similarity_vecotr[idx])
						x = idx;
						
					if (j == window.app.gift.similarity_vecotr[idx])
						y = idx;	
				}
				
				//return matrix element if symbols were found
				if (x != -1 && y != -1)	
					return window.app.gift.similarity_matrix[x][y];
				
				var worst_fit = -1;
				//return the worst value if both symbols were not found
				if (x == -1 && y == -1)
				{
					for (var xidx = 0; xidx < window.app.gift.similarity_vecotr.length; xidx++)
						for (var yidx = 0; yidx < window.app.gift.similarity_vecotr.length; yidx++)
						{
							if (window.app.gift.similarity_matrix[xidx][yidx] < worst_fit)
								worst_fit = window.app.gift.similarity_matrix[xidx][yidx];
						}
				//return the worst value if one symbol was not found
				} else if (x == -1)
				{
					for (var xidx = 0; xidx < window.app.gift.similarity_vecotr.length; xidx++)
						if (window.app.gift.similarity_matrix[xidx][y] < worst_fit)
							worst_fit = window.app.gift.similarity_matrix[xidx][y];
				} else if (y == -1)
				{
					for (var yidx = 0; yidx < window.app.gift.similarity_vecotr.length; yidx++)
						if (window.app.gift.similarity_matrix[x][yidx] < worst_fit)
							worst_fit = window.app.gift.similarity_matrix[x][yidx];
				}
				
				return worst_fit;
			}
		}
	};
	
})(jQuery);


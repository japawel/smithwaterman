﻿(function()
{
	window.view=
	{
		init: function()
		{
			window.view.mapUI();
			window.view.appendWidgets();
			window.view.appendEvents();
		},
		
		mapUI: function()
		{
			window.view.gui=
			{
				system:
				{	
					popupInfo:
					{
						whole: $('#popupInfo'),
						message: $('#popupInfo .popup-text-details:first'),
						btn_ok: $('#popupInfo .btn-ok:first'),
					},
				},
				
				logs:
				{
					idx: 0,
				},
				
				labels:
				{
					first_sequence: $('#first-sequence-label'),
					second_sequence: $('#second-sequence-label'),
					linear_p: $('#linear-p-label'),
					gap_p_opening: $('#gap-p-opening-label'),
					gap_p_extension: $('#gap-p-extension-label'),
					match: $('#match-label'),
					nonmatch: $('#nonmatch-label'),
					matrix_values: $('#matrix-values-label'), 
				},
				
				fields:
				{
					first_sequence: $('#first-sequence'),
					second_sequence: $('#second-sequence'),
					algorithm: $('#select-algorithm'),
					log_panel: $('#log-panel'),
					demonstrate_panel: $('#demonstrate-panel'),
					linear_p: $('#linear-p'),
					gap_p_opening: $('#gap-p-opening'),
					gap_p_extension: $('#gap-p-extension'),
					match: $('#match'),
					nonmatch: $('#nonmatch'),
				},
				
				collapsibles:
				{
					basic_options: $('#basic-options-collapsible'),
					algorithm_options: $('#algorithm-options-collapsible'),
					
				},
				
				buttons:
				{
					btn_next_step: $('#next-step'),
					btn_start: $('#start'),
					btn_next_stage: $('#next-stage'),
					btn_go_to_end: $('#go-to-end'),
				},
				
				selects:
				{
					select_punishment: $('#select-punishment'),
					scoring_system: $('#scoring-system'),
					matrix_types: $('#matrix-types'),
					example_sequences: $('#select-example-sequences'),
				},
			};
		},
		
		appendWidgets: function()
		{
			window.view.gui.system.popupInfo.whole.popup();
			window.view.gui.system.popupInfo.btn_ok.button();
			
			$(function () {
				$.widget("ui.tooltip", $.ui.tooltip, {
					options: {
						content: function () {
							return $(this).prop('title');
						}
					}
				});

				$(document).tooltip({
					position: {
						my: "center bottom-20",
						at: "center top",
						using: function( position, feedback ) {
						$( this ).css( position );
						$( "<div>" )
							.addClass( "arrow" )
							.addClass( feedback.vertical )
							.addClass( feedback.horizontal )
							.appendTo( this );
					}
				  }
				});
			});
			window.view.helpfulFunctions.initSimilarityMatrix();
		},
		
		appendEvents: function()
		{
			window.view.gui.fields.first_sequence.on('change', window.view.handlers.handleSequenceToUpperCase);
			window.view.gui.fields.second_sequence.on('change', window.view.handlers.handleSequenceToUpperCase);
			window.view.gui.buttons.btn_start.on('click', window.view.handlers.handleStartBtn);
			window.view.gui.buttons.btn_go_to_end.on('click', window.view.handlers.handleGoToEndBtn);
			window.view.gui.buttons.btn_next_step.on('click', window.view.handlers.handleNextStepBtn);
			window.view.gui.buttons.btn_next_stage.on('click', window.view.handlers.handleNextStageBtn);
			window.view.gui.selects.select_punishment.on('change',window.view.handlers.handleHideOptionsPunish);
			window.view.gui.selects.scoring_system.on('change',window.view.handlers.handleHideOptionsScore);
			window.view.gui.selects.matrix_types.on('change',window.view.handlers.handleHideOptionsMatrix);
			window.view.gui.selects.example_sequences.on('change',window.view.handlers.handleSelectExampleSequence);
		},
		
		
	
	
		handlers:
		{
			handleSelectExampleSequence: function()
			{
				if(window.view.gui.selects.example_sequences.val() == 1)
				{
					window.view.gui.fields.first_sequence.val("ACGT");
					window.view.gui.fields.second_sequence.val("ACTCGT")
				} else if(window.view.gui.selects.example_sequences.val() == 2)
				{
					window.view.gui.fields.first_sequence.val("GATGAA");
					window.view.gui.fields.second_sequence.val("GAAAGAT")
				}
			},
			/*handleSetTds: function()
			{
				$("#aa").defaultValue(4);
			}
			*/
			handleSequenceToUpperCase: function()
			{
				var self = $(this);
				self.val(self.val().toUpperCase());
			},
			
			handleStartBtn: function()
			{
				window.view.gui.fields.log_panel.text("");
				var isAnyToComplete = false;
				
				if (window.view.helpfulFunctions.markIfIsEmpty(
						window.view.gui.fields.first_sequence.val(), 
						window.view.gui.labels.first_sequence)
					) 
				{
					isAnyToComplete = true;
				} 
				if(window.view.helpfulFunctions.markIfIsEmpty(
						window.view.gui.fields.second_sequence.val(), 
						window.view.gui.labels.second_sequence)
					)
				{
					isAnyToComplete = true;
				}
				if(window.view.gui.selects.select_punishment.val() == 1
					&& window.view.helpfulFunctions.markIfIsEmpty(
						window.view.gui.fields.gap_p_opening.val(), 
						window.view.gui.labels.gap_p_opening, true)
					)
				{
					isAnyToComplete = true;
				}
				if(window.view.gui.selects.select_punishment.val() == 1
					&& window.view.helpfulFunctions.markIfIsEmpty(
						window.view.gui.fields.gap_p_extension.val(), 
						window.view.gui.labels.gap_p_extension, true)
					)
				{
					isAnyToComplete = true;
				}
				if(window.view.gui.selects.select_punishment.val() == 2
					&& window.view.helpfulFunctions.markIfIsEmpty(
						window.view.gui.fields.linear_p.val(), 
						window.view.gui.labels.linear_p, true)
					)
				{
					isAnyToComplete = true;
				}
				if(window.view.gui.selects.scoring_system.val() == 2
					&& window.view.helpfulFunctions.markIfIsEmpty(
						window.view.gui.fields.match.val(), 
						window.view.gui.labels.match, true)
					)
				{
					isAnyToComplete = true;
				}
				if(window.view.gui.selects.scoring_system.val() == 2
					&& window.view.helpfulFunctions.markIfIsEmpty(
						window.view.gui.fields.nonmatch.val(), 
						window.view.gui.labels.nonmatch, true)
					)
				{
					isAnyToComplete = true;
				}
				if(window.view.gui.selects.scoring_system.val() == 1
					&& window.view.gui.selects.matrix_types.val() == 2
				)
				{
					var matrixValues = [parseInt($("#aa").val()), parseInt($("#ag").val()), parseInt($("#ac").val()), parseInt($("#at").val()),
					                     parseInt($("#ga").val()), parseInt($("#gg").val()), parseInt($("#gc").val()), parseInt($("#gt").val()),
					                     parseInt($("#ca").val()), parseInt($("#cg").val()), parseInt($("#cc").val()), parseInt($("#ct").val()),
					                     parseInt($("#ta").val()), parseInt($("#tc").val()), parseInt($("#tg").val()), parseInt($("#tt").val())]
					var isAnyEmtyValuInMatrix = false;
					for (var i = 0; i < matrixValues.length; i++)
						if (isNaN(matrixValues[i]))
						{
							isAnyEmtyValuInMatrix = true;
							break;
						}
					 if (isAnyEmtyValuInMatrix)
					 {
						isAnyToComplete = true;
						window.view.gui.labels.matrix_values.addClass('to-complete-fields');
					 } else
					 {
						window.view.gui.labels.matrix_values.removeClass('to-complete-fields');
					 }
				}
				
				if (isAnyToComplete)
				{
					var msg = "Nie uzupełniono wszystkich wymaganych pól lub ich wartości nie są poprawne.";
					window.app.system.showInfoDialog(msg);
					return;
				}
				window.view.gui.logs.idx=0;
				window.view.gui.buttons.btn_next_step.button('enable');
				window.view.gui.buttons.btn_go_to_end.button('enable');
				window.view.gui.buttons.btn_next_stage.button('enable');
				window.view.gui.buttons.btn_start.button('disable');
				//window.view.gui.fields.algorithm.selectmenu('disable');
				window.view.gui.fields.first_sequence.addClass('ui-disabled');
				window.view.gui.fields.second_sequence.addClass('ui-disabled');
				window.view.gui.fields.gap_p_opening.addClass('ui-disabled');
				window.view.gui.fields.gap_p_extension.addClass('ui-disabled');
				
				window.view.gui.selects.scoring_system.addClass('ui-disabled');
				window.view.gui.selects.select_punishment.addClass('ui-disabled');
				window.view.gui.selects.matrix_types.addClass('ui-disabled');
				window.view.gui.selects.example_sequences.addClass('ui-disabled');
				
				window.view.gui.fields.linear_p.addClass('ui-disabled');
				window.view.gui.fields.match.addClass('ui-disabled');
				window.view.gui.fields.nonmatch.addClass('ui-disabled');
				
				window.view.helpfulFunctions.makeSimilarityMatrixDisabled();
				window.view.gui.collapsibles.basic_options.collapsible( "collapse" );
				window.view.gui.collapsibles.algorithm_options.collapsible( "collapse" );
				
				window.view.helpfulFunctions.logInfo('Rozpoczęcie porównywania sekwencji.');
				
				
				//ustawienie wszystkich parametrów
				var punishmentParams = new Object();
				if (window.view.gui.selects.select_punishment.val() == 1)
				{
					punishmentParams.begin_punishment = parseInt(window.view.gui.fields.gap_p_opening.val());
					punishmentParams.continue_punishment = parseInt(window.view.gui.fields.gap_p_extension.val());
				} else
					punishmentParams.begin_punishment = parseInt(window.view.gui.fields.linear_p.val());
				window.app.punishment.setPunishments(punishmentParams);
				var giftParams = new Object();
				if (window.view.gui.selects.scoring_system.val() == 1)
				{
					giftParams.simple_gift_function = false;
					giftParams.similarity_matrix = window.view.helpfulFunctions.getSimilarityMatrix();
					giftParams.similarity_vecotr = ['A', 'G', 'C', 'T']
				} else
				{
					giftParams.simple_gift_function = true;
					giftParams.simple_fit = parseInt(window.view.gui.fields.match.val());
					giftParams.simple_mismatch = parseInt(window.view.gui.fields.nonmatch.val());
				}
				
				window.app.gift.setDataFromPrams(giftParams);
				window.app.smithWaterman.init(
						{first:window.view.gui.fields.first_sequence.val(), 
						secound:window.view.gui.fields.second_sequence.val()}
					);
					
				var matrix = window.view.helpfulFunctions.makeMatrix();
				window.view.gui.fields.demonstrate_panel.html(matrix);
				
				//wykonanie ruchów i znalezienie ścieżki
				//window.app.smithWaterman.move();
				//window.app.smithWaterman.find_path();
				
				
			},
			
			handleNextStepBtn: function()
			{
				if (!window.app.smithWaterman.state.allmoves)
				{
					window.app.smithWaterman.move(true);
					
					var info = window.app.smithWaterman.getMoveInfo(
									(window.app.smithWaterman.state.moveno-1)%window.app.smithWaterman.state.xsize, 
									Math.floor((window.app.smithWaterman.state.moveno-1)/window.app.smithWaterman.state.xsize)
								);
					window.view.helpfulFunctions.logInfo(
									info, 
									'font-size: 13px', 
									'p_id_' 
										+ ((window.app.smithWaterman.state.moveno-1)%window.app.smithWaterman.state.xsize+1)
										+ '_' 
										+ (Math.floor((window.app.smithWaterman.state.moveno-1)/window.app.smithWaterman.state.xsize)+1)
								);
				} else if (!window.app.smithWaterman.state.pathfound)
				{
					if (window.app.smithWaterman.path.nexts.length == 0)
						window.view.helpfulFunctions.logInfo("Sprawdzanie sekwencji o największym dopasowaniu...");
					window.app.smithWaterman.find_path(true);
				}
				if (window.app.smithWaterman.state.pathfound)
				{
					window.view.helpfulFunctions.printPath();
					window.view.helpfulFunctions.setViewAfterEnd();
				}
				
				var matrix = $(document.getElementById('socre-matrix'));
				window.view.helpfulFunctions.refreshMatrix(matrix);
				
			},
			
			handleNextStageBtn: function()
			{
				if (!window.app.smithWaterman.state.allmoves)
					window.app.smithWaterman.move(false);
				else if (!window.app.smithWaterman.state.pathfound)
				{
					if (window.app.smithWaterman.path.nexts.length == 0)
						window.view.helpfulFunctions.logInfo("Sprawdzanie sekwencji o największym dopasowaniu...");
					window.app.smithWaterman.find_path(false);
				}
				if (window.app.smithWaterman.state.pathfound)
				{
					window.view.helpfulFunctions.printPath();
					window.view.helpfulFunctions.setViewAfterEnd();
				}
				
				var matrix = $(document.getElementById('socre-matrix'));
				window.view.helpfulFunctions.refreshMatrix(matrix);
			},
			
			handleGoToEndBtn: function()
			{
			
				if (!window.app.smithWaterman.state.allmoves)
					window.app.smithWaterman.move(false);
				if (!window.app.smithWaterman.state.pathfound)
					window.app.smithWaterman.find_path(false);
				
				window.view.helpfulFunctions.printPath();
				
				var matrix = $(document.getElementById('socre-matrix'));
				window.view.helpfulFunctions.refreshMatrix(matrix);
				
				window.view.helpfulFunctions.setViewAfterEnd();
				
			},
			
			handleHideOptionsPunish: function()
			{
				var sort = $(this).val();
				$("#punish1, #punish2").hide();
				if(sort > 0)
				 {
					$("#punish"+sort).show();
				 }
			},
			
			handleHideOptionsScore: function()
			{
				var sort2 = $(this).val();
				$("#scoring1, #scoring2, #matrix-type").hide();
				if(sort2 > 0)
				 {
					$("#scoring"+sort2).show();
					if (sort2 ==1)
					{
						$("#matrix-type").show();
					}
				 }
			},
			
			handleHideOptionsMatrix: function()
			{
				var sort3 = $(this).val();
				//$("#custom-matrix").hide();
				if(sort3 > 0)
				{
					if (sort3 == 1)
					{
						window.view.helpfulFunctions.initSimilarityMatrix();
						window.view.helpfulFunctions.makeSimilarityMatrixDisabled();
					}

					if (sort3 == 2)
					{
						window.view.helpfulFunctions.makeSimilarityMatrixEnabled();
						
					}
					
					$("#custom-matrix").show();
				}
				 
			},
		},
		
		helpfulFunctions:
		{
			initSimilarityMatrix: function()
			{
				$("#aa").val(10);
				$("#ag").val(-1);
				$("#ac").val(-3);
				$("#at").val(-4);
				$("#ga").val($("#ag").val()); 
				$("#gg").val(7);
				$("#gc").val(-5);
				$("#gt").val(-3);
				$("#ca").val($("#ac").val());
				$("#cg").val($("#gc").val());
				$("#cc").val(9);
				$("#ct").val(0);
				$("#ta").val($("#at").val());
				$("#tg").val($("#gt").val());
				$("#tc").val($("#ct").val());
				$("#tt").val(8);
			},
			
			makeSimilarityMatrixDisabled: function()
			{
				$("#aa").textinput('disable');
				$("#ag").textinput('disable');
				$("#ac").textinput('disable');
				$("#at").textinput('disable');
				$("#ga").textinput('disable');
				$("#gg").textinput('disable');
				$("#gc").textinput('disable');
				$("#gt").textinput('disable');
				$("#ca").textinput('disable');
				$("#cg").textinput('disable');
				$("#cc").textinput('disable');
				$("#ct").textinput('disable');
				$("#ta").textinput('disable');
				$("#tg").textinput('disable');
				$("#tc").textinput('disable');
				$("#tt").textinput('disable');
			},
			
			makeSimilarityMatrixEnabled: function()
			{
				$("#aa").val(""); $("#aa").textinput('enable');
				$("#ag").val(""); $("#ag").textinput('enable');
				$("#ac").val(""); $("#ac").textinput('enable');
				$("#at").val(""); $("#at").textinput('enable');
				$("#ga").val(""); $("#ga").textinput('enable');
				$("#gg").val(""); $("#gg").textinput('enable');
				$("#gc").val(""); $("#gc").textinput('enable');
				$("#gt").val(""); $("#gt").textinput('enable');
				$("#ca").val(""); $("#ca").textinput('enable');
				$("#cg").val(""); $("#cg").textinput('enable');
				$("#cc").val(""); $("#cc").textinput('enable');
				$("#ct").val(""); $("#ct").textinput('enable');
				$("#ta").val(""); $("#ta").textinput('enable');
				$("#tc").val(""); $("#tc").textinput('enable');
				$("#tg").val(""); $("#tg").textinput('enable');
				$("#tt").val(""); $("#tt").textinput('enable');
			},
			
			makeSimilarityMatrixEnabledNotEmpty: function()
			{
				$("#aa").textinput('enable');
				$("#ag").textinput('enable');
				$("#ac").textinput('enable');
				$("#at").textinput('enable');
				$("#ga").textinput('enable');
				$("#gg").textinput('enable');
				$("#gc").textinput('enable');
				$("#gt").textinput('enable');
				$("#ca").textinput('enable');
				$("#cg").textinput('enable');
				$("#cc").textinput('enable');
				$("#ct").textinput('enable');
				$("#ta").textinput('enable');
				$("#tc").textinput('enable');
				$("#tg").textinput('enable');
				$("#tt").textinput('enable');
			},
			
			printPath: function()
			{
				//wypisanie wyników
				var fpaths = new Array();
				var spaths = new Array();
				for (var i = 0; i < window.app.smithWaterman.sequences.first.length; i++)
				{
					if (window.app.smithWaterman.matrix[i][0].prev_first_paths != undefined)
					{
						for (var p = 0; p < window.app.smithWaterman.matrix[i][0].prev_first_paths.length; p++)
						{
							fpath = window.app.smithWaterman.matrix[i][0].first_paths[p];
							spath = window.app.smithWaterman.matrix[i][0].secound_paths[p];
							var pathIsSubpath = false;
							for(var cntPaths = 0; cntPaths < fpaths.length; cntPaths++)
							{
								if(fpaths[cntPaths].indexOf(fpath) > -1 && spaths[cntPaths].indexOf(spath) > -1)
								{
									pathIsSubpath = true;
									break;
								}
							}
							if (!pathIsSubpath)
							{
								fpaths.push(fpath);
								spaths.push(spath);
							}
						}
					}
					
				}
				
				for (var i = 0; i < window.app.smithWaterman.sequences.secound.length; i++)
				{
					if (window.app.smithWaterman.matrix[0][i].prev_first_paths != undefined)
					{
						for (var p = 0; p < window.app.smithWaterman.matrix[0][i].prev_first_paths.length; p++)
						{
							fpath = window.app.smithWaterman.matrix[0][i].first_paths[p];
							spath = window.app.smithWaterman.matrix[0][i].secound_paths[p];
							var pathIsSubpath = false;
							for(var cntPaths = 0; cntPaths < fpaths.length; cntPaths++)
							{
								if(fpaths[cntPaths].indexOf(fpath) > -1 && spaths[cntPaths].indexOf(spath) > -1)
								{
									pathIsSubpath = true;
									break;
								}
							}
							if (!pathIsSubpath)
							{
								fpaths.push(fpath);
								spaths.push(spath);
							}
						}
					}
					
				}
				//var i = window.app.smithWaterman.path.nexts[window.app.smithWaterman.path.nexts.length-1].i;
				//var j = window.app.smithWaterman.path.nexts[window.app.smithWaterman.path.nexts.length-1].j;
				var info = "Sekwencje o największym dopasowaniu:";
				for (var p = 0; p < fpaths.length; p++)
				{
					info += '</br>';
					info += fpaths[p];
					info += '</br>';
					info += spaths[p];
					info += '</br>';
				}
				
				window.view.helpfulFunctions.logInfo(info);
			},
			
			setViewAfterEnd: function()
			{
				window.view.gui.buttons.btn_next_step.button('disable');
				window.view.gui.buttons.btn_go_to_end.button('disable');
				window.view.gui.buttons.btn_next_stage.button('disable');
				window.view.gui.buttons.btn_start.button('enable');
				//window.view.gui.fields.algorithm.selectmenu('enable');
				window.view.gui.fields.first_sequence.removeClass('ui-disabled');
				window.view.gui.fields.second_sequence.removeClass('ui-disabled');
				window.view.gui.fields.gap_p_opening.removeClass('ui-disabled');
				window.view.gui.fields.gap_p_extension.removeClass('ui-disabled');
				
				window.view.gui.selects.scoring_system.removeClass('ui-disabled');
				window.view.gui.selects.select_punishment.removeClass('ui-disabled');
				window.view.gui.selects.matrix_types.removeClass('ui-disabled');
				window.view.gui.selects.example_sequences.removeClass('ui-disabled');
				
				window.view.gui.fields.linear_p.removeClass('ui-disabled');
				window.view.gui.fields.match.removeClass('ui-disabled');
				window.view.gui.fields.nonmatch.removeClass('ui-disabled');
				
				window.view.helpfulFunctions.makeSimilarityMatrixEnabledNotEmpty();
				
				window.view.gui.collapsibles.basic_options.collapsible( "expand" );
				window.view.gui.collapsibles.algorithm_options.collapsible( "expand" );
				
				$(window).scrollTop(document.body.scrollHeight);
			},
			
			logInfo: function(info, style, id)
			{
				window.view.gui.fields.log_panel.append('<p' 
										+ (style != undefined ? ' style="' + style + '"' : '') 
										+ (id != undefined ? ' id="' + id + '"': '')
										+ ' class="' + ((window.view.gui.logs.idx++)%2==0 ? 'evenlinelog' : 'oddlinelog') + '"' 
										+ '>'+info+'</p>');
				window.view.gui.fields.log_panel.scrollTop(window.view.gui.fields.log_panel[0].scrollHeight);
			},
			
			makeMatrix: function()
			{
				var first = window.app.smithWaterman.sequences.first;
				var secound = window.app.smithWaterman.sequences.secound;
				
				var matrix = $(document.createElement('table'))
					.attr({'id' : 'socre-matrix'});
				for (var i = 0; i < secound.length + 1; ++i)
				{
					var row = $(document.createElement('tr'));
					row.appendTo(matrix);
					for (var j = 0; j < first.length + 1; ++j)
					{
						var cell = $(document.createElement('td'))
							.attr({'id' : 'id_' + j + '_' + i})
							.css({'width' : '35px', 'height' : '35px', 'padding' : '0'});
						cell.appendTo(row);
						cell.append(window.view.helpfulFunctions.makeMatrixCell());
					}
				}
				
				for (var i = 0; i < first.length; ++i)
				{
					var x = $(matrix.find('#id_' + (i + 1) + '_0'));
					x.html(first[i]);
				}
				
				for (var i = 0; i < secound.length; ++i)
				{
					var x = $(matrix.find('#id_0_' + (i + 1)));
					x.html(secound[i]);
				}
				window.view.helpfulFunctions.refreshMatrix(matrix);
				return matrix;
			},
			
			makeMatrixCell: function()
			{
				var cellTable = $(document.createElement('table'))
					.css({'border' : '0px', 'font-size' : '10px'})
					.attr({'id' : 'cell-matrix'});
				
				var row = $(document.createElement('tr')).appendTo(cellTable);
				$(document.createElement('td'))
					.attr({'id' : 'from-diagonal'})
					.addClass('matrix-cell')
					.css({'font-size' : '7px', 'font-color' : 'grey'})
					.appendTo(row);
				$(document.createElement('td'))
					.attr({'id' : 'from-top'})
					.addClass('matrix-cell')
					.css({'font-size' : '7px', 'font-color' : 'grey'})
					.appendTo(row);
				
				row = $(document.createElement('tr')).appendTo(cellTable);
				$(document.createElement('td'))
					.attr({'id' : 'from-left'})
					.addClass('matrix-cell')
					.css({'font-size' : '7px', 'font-color' : 'grey'})
					.appendTo(row);
				$(document.createElement('td'))
					.attr({'id' : 'max-value'})
					.addClass('matrix-cell')
					.css({'font-size' : '12px', 'font-weight' : 'bold'})
					.appendTo(row);
				
				return cellTable;
			},
			
			refreshMatrix: function(matrix)
			{
				var first = window.app.smithWaterman.sequences.first;
				var secound = window.app.smithWaterman.sequences.secound;
				
				for (var i = 0; i < secound.length; ++i)
				{
					for (var j = 0; j < first.length; ++j)
					{				
						var x = $(matrix.find('#id_' + (j + 1) + '_' + (i + 1)));
						
						if (window.app.smithWaterman.matrix[j][i].from_diagonal != undefined)
						{
							var info = window.app.smithWaterman.getMoveInfo(j ,i);
							x.attr({'title' : info});
						}
						x = $(x.find('#cell-matrix'));
						var cell = $(x.find('#from-diagonal')).html(window.app.smithWaterman.matrix[j][i].from_diagonal);
						if (window.app.smithWaterman.matrix[j][i].max_from_diagonal)
							cell.addClass('from-diagonal');
						cell = $(x.find('#from-top')).html(window.app.smithWaterman.matrix[j][i].from_top);
						if (window.app.smithWaterman.matrix[j][i].max_from_top)
							cell.addClass('from-top');
						cell = $(x.find('#from-left')).html(window.app.smithWaterman.matrix[j][i].from_left);
						if (window.app.smithWaterman.matrix[j][i].max_from_left)
							cell.addClass('from-left');
						$(x.find('#max-value')).html(window.app.smithWaterman.matrix[j][i].max);
						if (window.app.smithWaterman.matrix[j][i].path != undefined)
						{
							var x = $(matrix.find('#id_' + (j + 1) + '_' + (i + 1)));
							x.css({'background-color' : 'lightblue'})
						}
					}
				}
			},
			
			getSimilarityMatrix: function()
			{
				var matrix = new Array(4);
				for (var i = 0; i < 4; i++)
					matrix[i] = new Array(4)
				
				matrix[0][0] = parseInt($("#aa").val());
				matrix[0][1] = parseInt($("#ag").val());
				matrix[0][2] = parseInt($("#ac").val());
				matrix[0][3] = parseInt($("#at").val());
				matrix[1][0] = parseInt($("#ga").val());
				matrix[1][1] = parseInt($("#gg").val());
				matrix[1][2] = parseInt($("#gc").val());
				matrix[1][3] = parseInt($("#gt").val());
				matrix[2][0] = parseInt($("#ca").val());
				matrix[2][1] = parseInt($("#cg").val());
				matrix[2][2] = parseInt($("#cc").val());
				matrix[2][3] = parseInt($("#ct").val());
				matrix[3][0] = parseInt($("#ta").val());
				matrix[3][1] = parseInt($("#tc").val());
				matrix[3][2] = parseInt($("#tg").val());
				matrix[3][3] = parseInt($("#tt").val());
				
				return matrix;
			},
			
			markIfIsEmpty: function(field, label, shouldBeNumber)
			{
				if ("" == field || (shouldBeNumber && isNaN(parseInt(field))))
				{	
					label.addClass('to-complete-fields');
					return true;
				}
				
				label.removeClass('to-complete-fields');
				return false;
			},
		},
	};
	
})(jQuery);

